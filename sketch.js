// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/jEwAMgcCgOA
var jump;
var time;
var hit= false;
clr =false;
var lost= false;
var gravity;
var song;
var amp;
var button;
var player;
var volhistory = [];
var obstacles= []
var zero;
function toggleSong() {
  if (song.isPlaying()) {
    song.pause();
  } else {
    song.play();
  }
}

function centerCanvas() {
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
}

function preload() {
  song = loadSound('1.mp3');
}

function setup() {
	time=0;
	player = new Mover();
	jump = new Victor(0,-5)
	gravity = new Victor(0,0.3);
	zero = new Victor(0,0);
  cnv = createCanvas(800, 600);
  cnv.parent("box")
  
  background(255, 0, 200);
  button = createButton('toggle');
  button.mousePressed(toggleSong);
  song.play();
  amp = new p5.Amplitude(0.9)
}

function draw() {
	
  background(255);
  
  
  stroke(0)
  fill(0)
  line(0,height-1,width,height-1);
  var vol = amp.getLevel();
  stroke(0,100)
  line(0,height-250,width,height-250)

  volhistory.push(vol);
  stroke(100);
  
  push();
  time++
  var y = map(vol, 0, 1, 0, height);
  fill(155,155,100,100)
  rect(width/2-y/3,60,y*2/3,1)
  if(y>250&& time>70){
    	fill(0)
    	var obs = new obstacle(volhistory.length)
    	obstacles.push(obs)
    	clr= true
    	noFill()
    	time=0;
    }
   //print(time%70)
fill(0,130,130);
  	if(clr){
	
	textSize(31)
	text("BEAT JUMPER",width/2-102,50)
	}
	else{
	textSize(30)
  
  text("BEAT JUMPER",width/2-100,50)
	}
	if(lost){
		textSize(60)
		text("YOU DIED",width/2-150,250)
		
	}
  for (var i = 0; i < volhistory.length; i+=2) {
    var y = map(volhistory[i], 0, 1, 0, height);
    
    colorMode(HSB);
    fill(155,155,100,10)
    rect(i,height,2,-y);
    
   
    
  }
 

  pop();
  stroke(255);
  
  
  if (volhistory.length > width) {
    volhistory.splice(0, 2);
    for (var i = 0; i < obstacles.length; i++) {
  	obstacles[i].update()
  	//print(i)
}
  }

  stroke(255, 0, 0,100);
  line(volhistory.length, 0, volhistory.length, height);
  //ellipse(100, 100, 200, vol * 200);
  
  
  //PLAYER
  player.display()
  player.update()
  player.applyForce(gravity)
  player.checkedges()
  
  
  //OBSTACLES
  for (var i = 0; i < obstacles.length; i++) {
  	obstacles[i].show()
  	//print(i)
}
clr=false;
}














function Mover() {
  this.position = new Victor(30,600);
  this.velocity = new Victor(0,0);
  this.acceleration= new Victor(0,0)
  this.force = new Victor(0,0);
 
   this.applyForce= function(force) {
    this.force = force
    this.acceleration.add(this.force);
  }
 
  

  this.update = function() {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    this.acceleration=this.acceleration.multiply(zero)
    
  }
 
  this.display=function () {
    stroke(0);
    fill(141, 229, 139)
    rect(this.position.x-10,this.position.y,10,-20)
  }
 this.checkedges = function(){
 	if(this.position.y>height){
 		this.position.y=height;
 		this.acceleration.y=0;
 		this.velocity.y=0;
 	}
 	
 }
  
}

function keyPressed() {
  if (keyCode === CONTROL) {
    player.applyForce(jump)
  } 
}




function obstacle(x){
	this.x =x
	this.y =height;
this.show = function(){
	fill(255);
	if(clr){
	fill(100)
	
	}
	rect(this.x,this.y,10,-10);
	
	
	//print("showing")
}
this.update= function(){
	this.x-=2
	if(this.x<0){
		obstacles.splice(0,1)
		//print(obstacles.length)
		
	}
	if(this.x<player.position.x && this.x+10>player.position.x && player.position.y>height-10){
		song.pause()
		lost=true
	}
	
}	
	
}